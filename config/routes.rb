Rails.application.routes.draw do
  devise_for :users
  root to: "dashboard#index"

  get 'dashboard/search_giphy' => 'dashboard#search_giphy', as: :search_giphy

  resources :gifs, only: [:index, :create, :show]
  resources :users, only: [:show]

  delete 'gifs/remove' => 'gifs#remove', as: :remove
end
