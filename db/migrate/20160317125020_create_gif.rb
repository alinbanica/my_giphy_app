class CreateGif < ActiveRecord::Migration
  def change
    create_table :gifs do |t|
      t.belongs_to :user, null: false
      t.string :giphy_id, unique: true
      t.string :image
      t.string :slug
      t.string :url
      t.string :source
      t.datetime :import_datetime

      t.timestamps
    end

    add_index :gifs, :giphy_id
  end
end
