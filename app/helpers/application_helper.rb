module ApplicationHelper
  def show_action_button(gif)
    if gif['exists']
      view_gif_button(gif['id'])
    else
      button_tag 'Add', type: 'button', :class => 'add-gif btn btn-primary'
    end
  end

  def show_tags_input(exists)
    unless exists
      text_field_tag "tags", '', :class => 'tags', :disabled => exists
    end
  end

  def show_checkbox(exists)
    unless exists
      check_box_tag('select', value = '1', checked = false, options = {:class => 'select_gif'})
    end
  end

  def show_select_all
    check_box_tag('select_all', value = '1', checked = false, options = {})
  end

  def show_pagination(pages)
    content_tag(:div, will_paginate(pages), class: 'row')
  end

  def global_message(key, value)
    content_tag(:div, value, :class => "alert alert-#{key}", role: "alert")
  end

  def show_tag_list(tag_list)
    tag_list.join(', ') if tag_list.present?
  end

  def view_gif_button(id)
    link_to 'View', gif_path(id), role: 'button', :class => 'view-gif btn btn-info'
  end

  def remove_gif_button
    button_tag 'Remove', type: 'button', :class => 'remove-gif btn btn-danger'
  end
end
