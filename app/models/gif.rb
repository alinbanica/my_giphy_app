class Gif < ActiveRecord::Base
  belongs_to :user
  acts_as_taggable

  validates :giphy_id, uniqueness: { scope: :user_id, message: "Already exists" }
end