class GifsController < ApplicationController
  before_filter :authenticate_user!

  def create
    if params['gifs'].present?
      gifs_details = Giphy::ApiClient.get_gifs_by_id({
        :ids => params['gifs'].map{ |k,v| v['giphy_id'] }
      })

      gifs_by_id = {}
      gifs_details.each{ |x| gifs_by_id[x['id']] = x }

      params['gifs'].each do |k, v|
        gif = Gif.new
        gif.user_id = current_user.id
        gif.giphy_id = v['giphy_id']
        gif.tag_list = v['tags']

        gif.slug = gifs_by_id[v['giphy_id']]['slug']
        gif.image = gifs_by_id[v['giphy_id']]['image']
        gif.url = gifs_by_id[v['giphy_id']]['url']
        gif.source = gifs_by_id[v['giphy_id']]['source']
        gif.import_datetime = gifs_by_id[v['giphy_id']]['import_datetime']

        gif.save!
      end

      data = current_user.gifs
      render_response(data, :created)
    else
      render_response('Please select some gifs for your collection', :bad_request)
    end
  end

  def index
    page = (params[:page] || 1).to_i
    @gifs = current_user.gifs
              .paginate(:page => page, :per_page => GIFS_PER_PAGE)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def remove
    if params['gifs'].present?
      deleted_gifs = current_user.gifs.where(:giphy_id => params['gifs']).destroy_all
      render_response(deleted_gifs, :ok)
    else
      render_response('There is nothing to remove', :bad_request)
    end
  end

  def show
    @gif = current_user.gifs.where(:giphy_id => params[:id]).last
  end

  private
    def permitted_params
      params.permit(:user_id, :giphy_id, :tag_list)
    end
end