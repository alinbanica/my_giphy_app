require 'will_paginate/array'

class DashboardController < ApplicationController
  before_filter :authenticate_user!

  def index
    redirect_to new_user_session_path unless user_signed_in?
  end

  def search_giphy
    if params['q'].present?
      page = (params[:page] || 1).to_i

      get_gifs_response = Giphy::ApiClient.get_gifs({
        :q => params['q'],
        :limit => GIFS_PER_PAGE,
        :offset => page-1
      })

      total_count = get_gifs_response['pagination']['total_count']

      pagination_options = {
        :page => page,
        :per_page => GIFS_PER_PAGE,
        :total_entries => total_count
      }

      @gifs = get_gifs_response['data']
      @pages = (1..total_count).to_a.paginate(pagination_options)

      if user_signed_in?
        @gifs.each{|x| x['exists'] = current_user.gifs.map(&:giphy_id).include?(x['id'])}
      end
    end

    respond_to do |format|
      format.js
    end
  end
end