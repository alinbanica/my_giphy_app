class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  GIFS_PER_PAGE = 10

  rescue_from Exception do |exception|
    status = :internal_server_error # 500

    status = :not_found if exception.kind_of? ActiveRecord::RecordNotFound # 404
    status = :bad_request if exception.kind_of? ActiveRecord::RecordInvalid # 400

    if exception.kind_of? PG::NotNullViolation
      status = :bad_request
      message = 'Database field should NOT be NULL'
    end

    unless message.present?
      message = exception.message
    end

    render_response([message], status)
  end

  private

  def build_success_response(response_data)
    { success: true, data: response_data }
  end

  def build_error_repsonse(response_data)
    { success: false, errors: [response_data] }
  end

  def render_response(request_response, status = :ok)
    response = (status == :ok || status == :created) ? build_success_response(request_response) : build_error_repsonse(request_response)

    render :json => response, :status => status
  end
end
