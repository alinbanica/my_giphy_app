class Giphy::Gifs_by_id < Giphy::ApiConnector
  # Public: method to use on request
  def http_method
    :get
  end

  # Public: url to perform request
  def url
    ''
  end
end