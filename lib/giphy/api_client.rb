class Giphy::ApiClient
  def self.get_gifs(opts = {})
    giphy_response = Giphy::Search.execute(opts)

    if is_success?(giphy_response)
      giphy_response['body']['data'].each do |x|
        image = x['images']['fixed_width']['url']
        x.slice!('id')
        x.merge!('image' => image)
      end

      success_response(giphy_response, ['data', 'pagination'])
    else
      error_response(giphy_response)
    end
  end

  def self.get_gifs_by_id(opts = {})
    if opts[:ids].kind_of? Array
      opts[:ids] = opts[:ids].join(',')
    end

    giphy_response = Giphy::Gifs_by_id.execute(opts)

    if is_success?(giphy_response)
      giphy_response['body']['data'].each do |x|
        image = x['images']['fixed_width']['url']
        x.slice!('id', 'slug', 'url', 'source', 'import_datetime')
        x.merge!('image' => image)
      end
    else
      error_response(giphy_response)
    end
  end

  private
    def self.is_success?(response)
      response['success']
    end

    def self.error_response(response)
      response
    end

    def self.success_response(response, keys = ['data'])
      { 'success' => response['success'] }.merge!( response['body'].slice(*keys) )
    end
end