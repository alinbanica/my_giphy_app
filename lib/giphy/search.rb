class Giphy::Search < Giphy::ApiConnector
  # Public: method to use on request
  def http_method
    :get
  end

  # Public: url to perform request
  def url
    '/search'
  end
end