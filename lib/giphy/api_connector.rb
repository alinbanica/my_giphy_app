class Giphy::ApiConnector
    API_KEY = 'dc6zaTOxFJmzC'

    def initialize(params={})
      @params = params.with_indifferent_access
      @params.merge!({:api_key => API_KEY})
    end

    # Public: method to use, should be overriden by subclass
    def http_method
      raise NotImplementedError
    end

    # Public: url to perform request to, should be overriden by subclass
    def url
      raise NotImplementedError
    end

    # Public: server_url to perform request to, should be overriden by subclass
    def server_url
      'http://api.giphy.com/v1/gifs'
    end

    # Public: full_url of the request
    def full_url
      server_url + url
    end

    def request_params
      @params
    end

    def method_required_params(keys = [])
      keys
    end

    def required_params
      req_params = [:api_key]
      req_params + method_required_params
    end

    def has_required_params?
      check_required_params(required_params)
    end

    def full_headers
      http_method == :get ? {} : {"Content-Type" => "application/json"}
    end

    # Public: whether to consider the status succesful
    # status - Integer representing the status code
    # returns Boolean
    def self.response_is_success?(status)
      (200..299).cover?(status)
    end

    # Public: main method responsible of performing the call
    def call
      if has_required_params?
        conn = Faraday.new(:url => server_url) do |faraday|
          faraday.request  :url_encoded             # form-encode POST params
          faraday.response :logger                  # log requests to STDOUT
          faraday.response :json, :content_type => /\bjson$/
          faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        end

        conn_method = conn.method(http_method)
        conn_method.call(full_url, request_params, full_headers)
      else
        return { 'success' => false, 'error' => "Required params not sent for #{url}" }
      end
    end

    def self.execute(params = {})
      obj = new(params)
      response = obj.call

      puts "Call to #{obj.url} returned #{response.inspect}"

      unless response.instance_of?(Hash)
        resp = { 'success' => false, 'body' => response.body }
        resp['success'] = true if self.response_is_success?(response.status)
        response = resp.deep_dup
      end

      response
    end

    private
      def check_required_params(keys = [])
        keys.all?{ |key| @params.include?(key) && @params[key].present? }
      end
end